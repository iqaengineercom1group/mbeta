Feature: Check all registration block
Need check display all components in Registration block

  Background:
    Given Open beta.mbeta.info home page
    When Click on Cadastre-se grátis agora! link
    Then Registration page is opened successfully

  Scenario: Check display all components in Registration block
    Then Display step-box
    Then Display Vamos começar? label
    Then Display Qual o seu gênero? label and selectbox
    Then Display Está em busca de quê? label and selectbox
    Then Display Eu sou... label and selectbox
    Then Display Continuar cadastro button





