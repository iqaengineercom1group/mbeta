import { Given,When,Then } from "cypress-cucumber-preprocessor/steps";

Given ('Open beta.mbeta.info home page', () =>{
    cy.visit('/');
    cy.url().should('include', '/initial/login')
});
When ('Click on Cadastre-se grátis agora! link', () =>{
        cy.contains('Cadastre-se grátis agora!')
            .should ('be.visible')
            .and ('contain', 'Cadastre-se grátis agora')
            .click({force:true})
});
Then('Registration page is opened successfully', () =>{
    cy.url()
        .should('include', '/cadastro')
    cy.contains('Faça parte da maior Rede Sugar')
        .should ('contain', "Faça parte da maior Rede Sugar")


});