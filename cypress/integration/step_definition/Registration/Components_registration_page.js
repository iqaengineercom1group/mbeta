import { Given,When,Then } from "cypress-cucumber-preprocessor/steps";


    Then ('Display step-box', () =>{
        cy.get('.step-box')
            .should('be.visible')
});
    Then ('Display Vamos começar? label', ()=> {
        cy.contains('Vamos começar?')
            .should ('contain', 'Vamos começar?')
});
    Then ('Display Qual o seu gênero? label and selectbox', () =>{
        cy.contains('Qual o seu gênero?')
            .should('contain', 'Qual o seu gênero?');
        cy.get('#genderSelect')
            .should('be.visible')
            .select('Masculino').should('have.value', '1: 1')
        cy.get('#genderSelect')
            .should('be.visible')
            .select('Feminino').should('have.value', '2: 8')
    });
    Then ('Display Está em busca de quê? label and selectbox', () =>{
        cy.contains ('Está em busca de quê?')
            .should ('be.visible')
            .and ('contain', 'Está em busca de quê?')

});
    Then ('Display Eu sou... label and selectbox', () =>{


});
    Then ('Display Continuar cadastro button', () =>{


    });